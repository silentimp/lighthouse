const puppeteer = require('puppeteer');
const lighthouse = require('lighthouse');
const url = 'https://www.werkspot.nl/inloggen/';

(async ()=>{
    const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox', '--headless']});
    const {lhr: {audits}} = await lighthouse(url, {
        port: (new URL(browser.wsEndpoint())).port,
        output: 'json',
        logLevel: 'info',
    }, {
        extends: 'lighthouse:default',
        settings: {
          onlyCategories: ['performance'],
        },
    });

    const metrics = [
      "speed-index", 
      "total-byte-weight", 
      "first-meaningful-paint", 
      "time-to-first-byte",
    ];

    const propsName = {
      "speed-index": "score", 
      "total-byte-weight": "rawValue", 
      "first-meaningful-paint": "score", 
      "time-to-first-byte": "rawValue", 
    };

    const direction = {
      "speed-index": "larger", 
      "total-byte-weight": "smaller", 
      "first-meaningful-paint": "larger", 
      "time-to-first-byte": "smaller", 
    };

    process.stdout.write(JSON.stringify([{
      "subject": '/inloggen',
      "metrics": metrics.map(metric => ({
          "name": audits[metric].title,
          "value": (audits[metric][propsName[metric]]).toFixed(1),
          "desiredSize": direction[metric],
      }))
    }]));

    if (audits["speed-index"].score < 0.5) {
        process.exit(1);
      } else {
        process.exit(0);
      }
    await browser.close();
})();